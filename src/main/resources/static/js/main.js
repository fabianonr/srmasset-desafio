var srmApp = angular.module("srmApp", ['ngRoute']);

srmApp.config(function($routeProvider, $locationProvider){	
	$routeProvider.when("/cadastro", {templateUrl: 'partials/cliente.html', controller: 'ClienteController'})
	$routeProvider.when("/clientes", {templateUrl: 'partials/listagem.html', controller: 'ListagemClienteController'})
	.otherwise({ redirectTo: '/' });
	
	$locationProvider.html5Mode(true);
});