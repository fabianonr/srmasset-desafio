srmApp.controller('ListagemClienteController', function($scope, $http){
	$scope.clientes = [];
	
	 $scope.carregarClientes = function(){  
		   $http({method:'GET', url:'http://localhost:8080/clientes'})
		   .then(function(response){
			   $scope.clientes=response.data;
			   console.log($scope.clientes);
		   },
		   function (response){
			   console.log(response.data);
			   console.log(response.status);
		   });	  
	   };
	   
	   $scope.carregarClientes();
});