srmApp.controller('ClienteController', function($scope, $http){

	$scope.clientes = [];
	$scope.cliente = {};
	
   $scope.carregarClientes = function(){  
	   $http({method:'GET', url:'http://localhost:8080/clientes'})
	   .then(function(response){
		   $scope.clientes=response.data;
		   console.log($scope.clientes);
	   },
	   function (response){
		   console.log(response.data);
		   console.log(response.status);
	   });	  
   };
   
   $scope.salvarClientes = function(){
	   $http({method:'POST', url:'http://localhost:8080/clientes', data:$scope.cliente} )
	   .then(function(response){
		   var cliente = response.data;
		   console.log(cliente);
		   $scope.clientes.push(cliente);
	   }, function(response){
		   console.log(response.data);
		   console.log(response.status);
	   });
   };
   
   $scope.excluirCliente = function( cliente ){
	   $http({method:'DELETE', url:'http://localhost:8080/clientes/' + cliente.id } )
	   .then(function(response){
		   
		   var pos = $scope.clientes.indexOf(cliente);
		   $scope.clientes.splice(pos, 1);
		   
	   }, function(response){
		   console.log(response.data);
		   console.log(response.status);
	   });
   };
   
   
   $scope.carregarClientes();
});

