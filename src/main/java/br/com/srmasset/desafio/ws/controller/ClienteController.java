package br.com.srmasset.desafio.ws.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.srmasset.desafio.ws.model.Cliente;
import br.com.srmasset.desafio.ws.service.ClienteService;

@RestController
public class ClienteController {
	
	@Autowired
	ClienteService service;
	
	@RequestMapping(method=RequestMethod.GET, value="/clientes",
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Cliente>> buscarTodosClientes(){
		Collection<Cliente> todosClientes = service.buscarClientes();
		return new ResponseEntity<>(todosClientes, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/clientes",
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> cadastrarCliente(@RequestBody Cliente cliente){
		Cliente clienteCadastrado = service.cadastrar(cliente);
		return new ResponseEntity<>(clienteCadastrado, HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/clientes/{id}")
	public ResponseEntity<Cliente> excluirClienteClientes(@PathVariable Long id){
		Cliente clienteEncontrado = service.buscarPorId(id);
		
		if( clienteEncontrado == null ){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else{
			service.excluir(clienteEncontrado);
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/clientes",
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> alterarCliente(@RequestBody Cliente cliente){
		Cliente clienteAlterado = service.alterar(cliente);
		return new ResponseEntity<>(clienteAlterado, HttpStatus.OK);
	}
}
