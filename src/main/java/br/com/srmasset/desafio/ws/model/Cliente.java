package br.com.srmasset.desafio.ws.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Cliente {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String nome;
	private Double limiteCredito;
	@Transient
	private String risco;
	private Double taxaJuros;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Double getLimiteCredito() {
		return limiteCredito;
	}
	
	public void setLimiteCredito(Double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}
	
	public String getRisco() {
		return risco;
	}
	
	public void setRisco(String risco) {
		this.risco = risco;
	}
	
	public Double getTaxaJuros() {
		return taxaJuros;
	}
	
	public void setTaxaJuros(Double taxaJuros) {
		this.taxaJuros = taxaJuros;
	}
}
