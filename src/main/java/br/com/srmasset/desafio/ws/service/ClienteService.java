package br.com.srmasset.desafio.ws.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.srmasset.desafio.ws.model.Cliente;
import br.com.srmasset.desafio.ws.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	ClienteRepository repository;
		
	public Cliente cadastrar(Cliente cliente){	
		
		if( cliente.getRisco().equals("B") ){
			cliente.setTaxaJuros(10.0);
		}else if(cliente.getRisco().equals("C")){
			cliente.setTaxaJuros(20.0);
		}
		
		return repository.save(cliente);
	}
	
	public Collection<Cliente> buscarClientes(){
		return repository.findAll();
	}
	
	public void excluir(Cliente cliente){
		repository.delete(cliente);
	}
	
	public Cliente buscarPorId(Long id) {
		return repository.findOne(id);
	}
	
	public Cliente alterar(Cliente cliente){
		return repository.save(cliente);
	}

}
