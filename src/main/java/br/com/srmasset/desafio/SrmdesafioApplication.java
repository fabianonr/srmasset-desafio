package br.com.srmasset.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrmdesafioApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrmdesafioApplication.class, args);
	}
}
