# Desafio SRM - Cadastro de clientes


# Requisitos

 - jre1.8.0_111 ou superior
 - Banco de dados MySQLServer 5.6
 - Apache Tomcat 8 ou superior

## Instalação

Fazer deploy no Tomcat do arquivo apiweb-0.0.1-SNAPSHOT.war, localizado na raiz do projeto.
Instalar banco de dados com MySQL e configurar para porta defaul: 3306 com schema chamado: srmassetdb


## Tecnologia utilizada


 - Java 8
 - Spring Boot
 - Spring CDI
 - JPA
 - Maven
 - AngularJS 1.5.7
 - Bootstrap 3
 - MySql 5.6
 - Apache tomcat
